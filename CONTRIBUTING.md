## Terms

This vulnerability database is available freely under the [GitLab Security Alert Database Terms](./TERMS.md), please review them before contributing.

_This notice should stay as the first item in the CONTRIBUTING.md file._

## Submit a vulnerability

**Search the project** before submitting a new vulnerability.
The vulnerability may already in repo. If not, there may be a Merge Request or an issue for it,
and you may show your support with an award emoji and/or join the discussion.

If there's no match in the project, we encourage you to **submit a Merge Request**
to add a new vulnerability to the database. See how to [contribute a YAML file](#contribute-a-yaml-file).

The title of the Merge Request should be like:

> Add CVE-2019-11324 to urllib3

Where "CVE-2019-11324" is the identifier of the advisory and "urllib3" is the package name.

The description of the Merge Request must contain a link to a security advisory,
to the affected source code or to the fix.

Alternatively you may create an issue with the same information (title and description)
but your request is more likely to be addressed quickly if you submit a Merge Request instead,
even if the submitted YAML file doesn't contain all the details.

It's possible that the vulnerability already exists in the repo
but with a **different identifier or package name**.
You may then want to open a Merge Request to challenge the contents and/or the path
of the YAML file describing the vulnerability.

## Contribute a YAML file

Please checks these before submitting a YAML file to the repo:

* `identifier` should be the CVE id when it exists.
* `package_slug` refers to a public package.
* `title` must not contain the package name.
* `date` is the date on which the advisory was made public.
* `not_impacted` must not list the fixed versions.
* `solution` (string, optional): How to remediate the vulnerability.
* `urls` must contain URLs specific to the vulnerability, not URLs generic to the package itself.
* `uuid` must be `null` or omitted. It's set when publishing the advisory on Gemnasium.

`description` must not contain:
* an overview of the package itself, only the vulnerability belonging to the package
* fixed versions - this is redundant with `fixed_versions`
* affected versions - this is redundant with `affected_versions`
* solution to remediate the vulnerability - use `solution` instead
* links - use `urls` instead

Since the `description` supports [Markdown](https://daringfireball.net/projects/markdown) syntax,
[backtick quotes](https://daringfireball.net/projects/markdown/syntax#precode)
should be used for variables, constants, functions, class names and file paths.

Gemnasium also performs checks when submitting an issue or requesting a preview,
including checks on the fixed and affected versions:

* `fixed_versions` must match versions that exist in Gemnasium DB.
* `affected_range` must be a valid range and contains no fixed versions.

### Affected package

Contributors should carefully review the package name and make sure this is where the vulnerability has been found.
It happens that a vulnerability is reported in the context of a meta-package
like the Ruby gem [rails](https://rubygems.org/gems/rails) when in fact it comes from one of its dependencies like
[actionpack](https://rubygems.org/gems/actionpack) or [actionview](https://rubygems.org/gems/actionview).
This is a common issue when submitting vulnerabilities for Java Maven packages.
If unsure about the affected Maven package,
you can use [mvnrepository.com](https://mvnrepository.com/) to investigate packages and their dependencies.

### Affected & fixed versions

Contributors should carefully review `fixed_versions` and `affected_range`
and make sure they are consistent with the information that's publicly available:
- advisories published on [NVD](https://nvd.nist.gov), [CVE](https://cve.mitre.org/) or others
- affected and fixed commits in the package's source repository

So whenever possible, contributors should make sure affected and fixed commits in the repo
correspond to affected and fixed versions of the package:
- if package versions correspond to git tags, check the tags of the commits fixing the vulnerability
- if the repo features a changelog, look for commits or branches containing the fix,
  and see what version is added to the changelog

If there's no easy way to make the connection between the package versions and the source code,
it may be necessary to download the package at specific versions, open it,
and see whether it contains the fix or the vulnerable code.

**Look for backports** of the fix. Popular projects usually support multiple branches, and security fixes are backported.

**Beware of unsupported branches.**
These unsupported branches may not be explicitly mentioned in the security advisory even though they are affected.
If you can't establish whether some old versions are affected, then you should consider they are;
it's better to report a false positive than ignoring the risk.
